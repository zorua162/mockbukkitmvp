import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.OfflinePlayerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import me.zorua162.mockbukkitmvp.MockBukkitMVP;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.junit.jupiter.api.*;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestMockBukkitMVP {
    private ServerMock server;
    private MockBukkitMVP plugin;

    private PlayerMock player;

    @BeforeEach
    public void setUp() {
        // Start the mock server
        server = MockBukkit.mock();
        // Load your plugin
        plugin = MockBukkit.load(MockBukkitMVP.class);
    }

    @AfterEach
    public void tearDown() {
        // Stop the mock server
        MockBukkit.unmock();
    }

    @Test
    public void performYourTest() {
        // Perform your test
        PlayerMock playerMock = server.addPlayer();

        playerMock.disconnect();
        playerMock.reconnect();

        assertTrue(playerMock.hasPlayedBefore());

    }
}
